<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Segment;
use App\Exports\SegmentExport;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Authorization routes
 */
Route::post('auth/token', 'AuthController@createToken');

/**
 * All standard API routes, secured with a Bearer token
 */
Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('me', 'UsersController@showMe');
    });

    Route::prefix('tiles')->group(function () {
        Route::get('', 'TilesController@viewAny');
        Route::post('', 'TilesController@create');
        Route::put('{id}', 'TilesController@update');
    });

    Route::prefix('vehicle')->group(function () {
        Route::get('', 'VehicleController@showMyVehicle');
        Route::put('', 'VehicleController@update');
    });


    Route::prefix('attributes')->group(function () {
        Route::get('', 'AttributesController@viewAny');
        Route::post('', 'AttributesController@create');
        Route::get('{key}', 'AttributesController@view');
        Route::put('{key}', 'AttributesController@update');
        Route::post('{key}/value', 'AttributesController@sendValue');
    });

    Route::prefix('segments')->group(function () {
        Route::get('', 'SegmentsController@viewAny');
        Route::post('', 'SegmentsController@create');
        Route::get('{id}', 'SegmentsController@view');
        Route::get('{id}/export', 'SegmentsController@export');
        Route::get('{id}/logs', 'SegmentsController@logs');
        Route::put('{id}', 'SegmentsController@update');

        Route::get('{id}/logs/time_series', 'SegmentsController@getTimeSeries');
        Route::get('{id}/logs/scatter', 'SegmentsController@getScatter');
    });

    Route::prefix('logs')->group(function () {
        Route::get('latest', 'LogsController@getLatest');
        Route::post('file', 'LogsController@createFromFile');
    });
});

/**
 * Public routes for a specific vehicle
 */
Route::prefix('public/vehicles/{channel}')->group(function () {
    Route::get('attributes', 'PublicVehicleController@viewAttributes');
    Route::get('tiles', 'PublicVehicleController@viewTiles');
});

/**
 * Routes for realtime publishing (using Fanout.io)
 */
Route::prefix('realtime')->group(function () {
    Route::post('token', 'RealtimeController@createToken')->middleware('auth:sanctum');
    Route::get('private/{token}', 'RealtimeController@registerToPrivateChannel');
    Route::get('public/{channel}', 'RealtimeController@registerToPublicChannel');

});

/**
 * Routes for incoming IoT Data
 */
Route::post('iot/logs', 'IoTController@createLog');

/**
 * Temporary "beun" endpoint
 * We're in a little bit of a hurry, okay...
 */
Route::get('/spectrum/avg_charge', function (Request $request) {
    date_default_timezone_set('Europe/Amsterdam');
    echo "<h1>Average charge</h1>";

    $onemin = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subMinutes(1),
            now()
        ])->first();
    echo "1 minute: " . ($onemin->cnt ?? 0) . " W";

    echo "<br><br>";
    $tenmin = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subMinutes(10),
            now()
        ])->first();
    echo "10 minutes: " . ($tenmin->cnt ?? 0) . " W";

    echo "<br><br>";
    $thirtymin = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subMinutes(30),
            now()
        ])->first();
    echo "30 minutes: " . ($thirtymin->cnt ?? 0) . " W";

    echo "<br><br>";
    $onehour = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subHours(1),
            now()
        ])->first();
    echo "1 hour: " . ($onehour->cnt ?? 0) . " W";

    echo "<br><br>";
    $twohours = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subHours(2),
            now()
        ])->first();
    echo "2 hours: " . ($twohours->cnt ?? 0) . " W";

    echo "<br><br>";
    $threehours = DB::table('logs')->select(DB::raw('avg(cast("values" ->> \'battery_power_charge\' AS float)) as cnt'))
        ->where('vehicle_id', 2)
        ->whereBetween('created_at',[
            now()->subHours(3),
            now()
        ])->first();
    echo "3 hours: " . ($threehours->cnt ?? 0) . " W";


    echo "<br><br><br><br>Last refresh at: " . now();
//    return "ok.";
});

/**
 * For testing purposes
 */
Route::get('test', function (Request $request) {
    return "ok.";
});
