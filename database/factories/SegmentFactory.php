<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Segment;
use Faker\Generator as Faker;

$factory->define(Segment::class, function (Faker $faker) {
    $end = $faker->dateTimeThisYear();
    return [
        'name' => $faker->streetName . ' ' . date($format = 'd-m-Y'),
        'vehicle_id' => $faker->randomElement(DB::table('vehicles')->pluck('id')),
        'start' => $faker->dateTimeThisYear($end),
        'end' => $end
    ];
});
