<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vehicle;
use Faker\Generator as Faker;

$factory->define(Vehicle::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'team_name' => $faker->company,
        'device_id' => Str::random(16),
        'public_channel' => Str::random(16),
        'private_channel' => Str::random(16),
    ];
});
