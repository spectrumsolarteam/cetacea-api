<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Attribute;
use Faker\Generator as Faker;

$factory->define(Attribute::class, function (Faker $faker) {
    $name = $faker->catchPhrase();
    return [
        'key' => preg_replace('/[^A-Za-z]/', '_', strtolower($name)),
        'vehicle_id' => $faker->randomElement(DB::table('vehicles')->pluck('id')),
        'name' => $name,
        'public' => $faker->randomElement([true, false]),
        'unit' => $faker->fileExtension(),
        'round' => $faker->numberBetween(0,3),
        'archived' => $faker->randomElement([true, false])
    ];
});
