<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Jurre',
            'email' => 'admin@cetacea.solar',
            // 'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role' => 'admin',
            'remember_token' => Str::random(10),
            'vehicle_id' => 1
        ]);
        \App\User::create([
            'name' => 'Chief',
            'email' => 'chief@cetacea.solar',
            // 'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role' => 'chief',
            'remember_token' => Str::random(10),
            'vehicle_id' => 1
        ]);
        factory(\App\User::class, 2)->create();
    }
}
