<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->string('key', 100);
            $table->unsignedBigInteger('vehicle_id');
            $table->string('name', 100);
            $table->boolean('public');
            $table->string('unit', 25);
            $table->tinyInteger('round')->nullable();
            $table->integer('upper_bound')->nullable();
            $table->integer('lower_bound')->nullable();
            $table->string('calculation', 512)->nullable();
            $table->boolean('archived');
            $table->timestamps();

            $table->primary(['key', 'vehicle_id']);
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
