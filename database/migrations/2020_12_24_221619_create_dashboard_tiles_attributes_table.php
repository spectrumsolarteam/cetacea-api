<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardTilesAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiles_attributes', function (Blueprint $table) {
            $table->unsignedBigInteger('tile_id');
            $table->unsignedBigInteger('attribute_id');

            $table->primary(['tile_id', 'attribute_id']);
            $table->foreign('tile_id')->references('id')->on('dashboard_tiles');
            $table->foreign('attribute_id')->references('id')->on('attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_tiles_attributes', function (Blueprint $table) {
            //
        });
    }
}
