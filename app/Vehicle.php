<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vehicles';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'name', 'team_name', 'device_id', 'public_channel', 'created_at', 'updated_at'
    ];

    protected $fillable = [
        'name', 'team_name'
    ];

    public function users() {
        return $this->hasMany('App\User');
    }

    /** Please see the relation "attrs" below */
    public function attributes()
    {
        return $this->hasMany('App\Attribute');
    }

    /**
     * As attributes is a reserved/magic word, we use this name for the relation.
     * Apparently, sometimes 'attributs' does acutally work and as it was already used at some places, we also have the relation "attributes" above
     */
    public function attrs()
    {
        return $this->hasMany('App\Attribute');
    }

    public function segments()
    {
        return $this->hasMany('App\Segment');
    }

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    public function tiles()
    {
        return $this->hasMany('App\Tile');
    }
}
