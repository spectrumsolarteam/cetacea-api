<?php

namespace App\Policies;

use App\Attribute;
use App\Vehicle;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AttributePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any attributes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isChief() || $user->isMember();
    }

    /**
     * Determine whether the user can view the attribute.
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function view(User $user, Attribute $attribute)
    {
        return ($user->isChief() || $user->isMember()) && $attribute->vehicle->is($user->vehicle);
    }

    /**
     * Determine whether the user can create attributes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isChief();
    }

    /**
     * Determine whether the user can update the attribute.
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function update(User $user, Attribute $attribute)
    {
        return $user->isChief() && $attribute->vehicle->is($user->vehicle);
    }

    /**
     * Determine whether the user can delete the attribute.
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function delete(User $user, Attribute $attribute)
    {
        //
    }

    /**
     * Determine whether the user can restore the attribute.
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function restore(User $user, Attribute $attribute)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the attribute.
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function forceDelete(User $user, Attribute $attribute)
    {
        //
    }

    /**
     * Determine whether the user can set a value for the attribute
     *
     * @param  \App\User  $user
     * @param  \App\Attribute  $attribute
     * @return mixed
     */
    public function value(User $user, Attribute $attribute)
    {
        return $attribute->vehicle->is($user->vehicle);
    }
}
