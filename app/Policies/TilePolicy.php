<?php

namespace App\Policies;

use App\Tile;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->isChief() || $user->isMember();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isChief();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Tile  $tile
     * @return mixed
     */
    public function update(User $user, Tile $tile)
    {
        return $user->isChief() && $tile->vehicle->is($user->vehicle);
    }
}
