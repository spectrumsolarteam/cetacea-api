<?php

namespace App\Policies;

use App\User;
use App\segment;
use Illuminate\Auth\Access\HandlesAuthorization;

class SegmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any segments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
    }

    /**
     * Determine whether the user can view the segment.
     *
     * @param  \App\User  $user
     * @param  \App\segment  $segment
     * @return mixed
     */
    public function view(User $user, segment $segment)
    {
        return ($user->isChief() || $user->isMember()) && $segment->vehicle->is($user->vehicle);
    }

    /**
     * Determine whether the user can create segments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isChief() || $user->isMember();
    }

    /**
     * Determine whether the user can update the segment.
     *
     * @param  \App\User  $user
     * @param  \App\segment  $segment
     * @return mixed
     */
    public function update(User $user, segment $segment)
    {
        return ($user->isChief() || $user->isMember()) && $segment->vehicle->is($user->vehicle);
    }

    /**
     * Determine whether the user can delete the segment.
     *
     * @param  \App\User  $user
     * @param  \App\segment  $segment
     * @return mixed
     */
    public function delete(User $user, segment $segment)
    {
        //
    }

    /**
     * Determine whether the user can restore the segment.
     *
     * @param  \App\User  $user
     * @param  \App\segment  $segment
     * @return mixed
     */
    public function restore(User $user, segment $segment)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the segment.
     *
     * @param  \App\User  $user
     * @param  \App\segment  $segment
     * @return mixed
     */
    public function forceDelete(User $user, segment $segment)
    {
        //
    }
}
