<?php

namespace App\Exports;

use App\Segment;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SegmentExport implements FromQuery, WithHeadings
{
    public function __construct(Segment $segment)
    {
        $this->segment = $segment;

        $this->collumns = [];
        $i = 1;
        foreach($segment->vehicle->attributes as $attribute) {
            $this->collumns[$attribute->key] = $i;
            $i++;
        }
    }

    public function headings(): array
    {
        $headings = [];
        $headings[0] = 'Timestamp';
        foreach($this->segment->vehicle->attributes as $attribute) {
            if(($this->collumns[$attribute->key] ?? 0) > 0) {
                $headings[$this->collumns[$attribute->key]] = $attribute->name . " (" . $attribute->unit . ")";
            }
        }
        return $headings;
    }

    public function query()
    {
        $cols = ['logs.created_at'];
        foreach($this->collumns as $key=>$i) {
            $cols[$i] = 'logs.values->'.$key.' as '.$key;
        }
        return $this->segment->logs()->select($cols);
    }

}
