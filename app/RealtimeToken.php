<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealtimeToken extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'realtime_tokens';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['token', 'channel', 'valid'];

    /**
     * Returns boolean if the token is still valid to use.
     *
     * @var boolean
     */
    public function isValid() {
        return $this->valid && (strtotime('now - 5 seconds') <= strtotime($this->created_at));
    }
}
