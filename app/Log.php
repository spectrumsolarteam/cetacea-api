<?php

namespace App;

use FormulaParser\FormulaParser;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    protected $casts = [
        'values' => 'object'
    ];

    protected $fillable = [
        'values', 'created_at'
    ];

    protected $hidden = [
        'laravel_through_key'
    ];

    public $timestamps = false;

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function valuesArray() {
        return json_decode($this->values);
    }

    public function publicValues()
    {
        $tiles = $this->vehicle->tiles()->with('attributes')->public()->get();

        $public_values = [];
        foreach($tiles as $tile) {
            foreach($tile->getRelatedAttributes() as $attribute) {
                $public_values[$attribute->key] = ($this->values->{$attribute->key} ?? '');
            }
        }
        return (object) $public_values;
    }

    public function scopeOfVehicle($query, Vehicle $vehicle)
    {
        $query->where('vehicle_id', $vehicle->id);
    }

    public static function validateInput(Vehicle $vehicle, $values) {

    }

    public static function calculateValues(Vehicle $vehicle, $input) : array
    {
        $values = [];

        foreach($vehicle->attrs as $attribute) {
            if((property_exists($input, $attribute->key) || $attribute->calculation != NULL) && !$attribute->archived) {
                $value = $input->{$attribute->key} ?? '';

                if($attribute->calculation != NULL) {
                    $calc = preg_replace_callback('/%%[a-z_-]*%%/',
                        function ($matches) use ($input) {
                            return $input->{str_replace('%%', '', $matches[0])} ?? 0;
                        },
                        $attribute->calculation);

                    $parser = new FormulaParser($calc, 10);
                    $result = $parser->getResult();
                    $value = $result[0] == 'done' ? $result[1] : 0;
                }

                if($attribute->round != NULL) {
                    $value = round($value, $attribute->round);
                }

                $values[$attribute->key] = $value;
            }
        }

        return $values;
    }
}
