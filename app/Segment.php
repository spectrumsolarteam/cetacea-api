<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'segments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'start', 'end'];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function attrs()
    {
        return $this->hasManyThrough(
            'App\Attribute',
            'App\Vehicle',
            'id',
            'vehicle_id',
            'vehicle_id',
            'id'
        );
    }

    public function logs()
    {
        /** This relation cannot be called in a "with" part, because at that moment $this->... attributes are NULL
         *  When calling ->logs()->get() afterward, everything works like a charm
         */
        return $this->hasManyThrough(
            'App\Log',
            'App\Vehicle',
            'id',
            'vehicle_id',
            'vehicle_id',
            'id'
        )
        ->where('logs.created_at', '>=', $this->start)
        ->where('logs.created_at', '<=', $this->end)
        ->orderBy('logs.created_at');
    }

//    public function retrieveLogs()
//    {
//        ray($this->vehicle_id);
//        return Log::where('vehicle_id', $this->vehicle_id)
//            ->where('created_at', '>=', $this->start)
//            ->where('created_at', '<=', $this->end)
//            ->get();
//    }
}
