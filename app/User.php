<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use App\Vehicle;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $visible = [
        'name', 'email', 'role', 'created_at', 'updated_at', 'vehicle'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'email_verified_at' => 'datetime',
    ];

    protected $table = 'users';

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function logs()
    {
        return $this->hasManyThrough(
            'App\Log',
            'App\Vehicle',
            'id',
            'vehicle_id',
            'vehicle_id',
            'id'
        );
    }

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function isChief()
    {
        return $this->role == 'chief';
    }

    public function isMember()
    {
        return $this->role == 'member';
    }
}
