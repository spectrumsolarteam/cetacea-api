<?php

namespace App\Broadcasting;

use Illuminate\Contracts\Broadcasting\Broadcaster;
use GripControl\GripPubControl;
use App\User;

class FanoutBroadcaster implements Broadcaster
{
    protected $fanout;

    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct($config)
    {
        $this->fanout = new GripPubControl(array(
            'control_uri' => 'https://api.fanout.io/realm/'.$config['realm_id'],
            'control_iss' => $config['realm_id'],
            'key' => base64_decode($config['realm_key'])));
    }

    /**
     * Authenticate the incoming request for a given channel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function auth($request)
    {

    }

    /**
     * Return the valid authentication response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $result
     * @return mixed
     */
    public function validAuthenticationResponse($request, $result)
    {

    }

    /**
     * Broadcast the given event.
     *
     * @param  array  $channels
     * @param  string  $event
     * @param  array  $payload
     * @return void
     */
    public function broadcast(array $channels, $event, array $payload = [])
    {        
        foreach ($channels as $channel) {
            $this->fanout->publish_http_stream($channel, "event: ".$event."\ndata: ".json_encode($payload[$channel] ?? '{}')."\n\n");
        }
    }
}
