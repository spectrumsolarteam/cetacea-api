<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vehicle;
use App\Attribute;
use Illuminate\Support\Facades\Redis;
use Google\Cloud\Iot\V1\DeviceManagerClient;
use Illuminate\Support\Facades\DB;

class AttributesController extends Controller
{
    public function viewAny(Request $request)
    {
        $r = $request->user()->vehicle->attributes();
        if(!$request->input('includeArchived') == true) {
            $r->unarchived();
        }
        return $r->orderBy('name')->get();
    }

    public function view(Request $request, String $key)
    {
        $attribute = $request->user()->vehicle->attributes()
                        ->where('key', $key)->firstOrFail();

        $this->authorize('view', $attribute);

        return $attribute;
    }

    public function create(Request $request)
    {
        $this->authorize('create', Attribute::class);

        $data = $request->validate([
            'key' => 'required|max:100',
            'name' => 'required|max:100',
            'unit' => 'required|string|max:25',
            'round' => 'required|integer|nullable',
            'calculation' => 'string|nullable',
            'public' => 'boolean|nullable',
            'lower_bound' => 'integer|nullable',
            'upper_bound' => 'integer|nullable',
        ]);

        $attribute = $request->user()->vehicle->attributes()->create($data);

        return $attribute;
    }

    public function update(Request $request, String $key)
    {
        $data = $request->validate([
            'name' => 'required|max:100',
            'unit' => 'required|string|max:25',
            'round' => 'required|integer|nullable',
            'calculation' => 'string|nullable',
            'public' => 'boolean|nullable',
            'lower_bound' => 'integer|nullable',
            'upper_bound' => 'integer|nullable',
        ]);

        $attribute = $request->user()->vehicle->attributes()
                        ->where('key', $key)->firstOrFail();

        $this->authorize('update', $attribute);

        $attribute->update($data);

        return $attribute;
    }

    public function sendValue(Request $request, String $key)
    {
        $data = $request->validate([
            'value' => 'required|string',
        ]);

        $attribute = Attribute::where('key', $key)->where('vehicle_id', $request->user()->vehicle_id)->firstOrFail();

        $this->authorize('value', $attribute);

        // send command using Google IoT sdk
        $device_id = $attribute->vehicle->device_id;
        $deviceManagerClient = new DeviceManagerClient([
            // 'credentials' => json_decode(file_get_contents('C:/Users/jurre/Downloads/cetacea-solar-40d1bb014c29.json'), true)
            'credentials' => json_decode(file_get_contents(storage_path() . '/cetacea-solar-40d1bb014c29.json'), true)
        ]);
        try {
            $formattedName = $deviceManagerClient->deviceName(
                env('GOOGLE_IOT_PROJECT'),
                env('GOOGLE_IOT_LOCATION'),
                env('GOOGLE_IOT_REGISTRY'),
                $device_id
            );
            $binaryData = $attribute->key . ':' . 'true'; //$data->value;
            $response = $deviceManagerClient->sendCommandToDevice($formattedName, $binaryData);
            return response('', 201);
        } catch(\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode() ?? 500);
        } finally {
            $deviceManagerClient->close();
        }
    }
}
