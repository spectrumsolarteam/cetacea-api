<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FormulaParser\FormulaParser;

use App\Log;
use App\Vehicle;

use App\Events\NewLogCreatedEvent;


class IoTController extends Controller
{
    public function createLog(Request $request)
    {
        $data = $request->validate([
            'message.attributes.deviceId' => 'required',
            'message.data' => 'required',
            'message.publishTime' => '',
        ]);
        $vehicle = Vehicle::firstWhere('device_id', $data['message']['attributes']['deviceId']);

        $values = [];
        $data_values = json_decode(base64_decode($data['message']['data']));
        foreach($vehicle->attributes as $attribute) {
            if((property_exists($data_values, $attribute->key) || $attribute->calculation != NULL) && !$attribute->archived) {
                $value = $data_values->{$attribute->key} ?? '';

                if($attribute->calculation != NULL) {
                    $calc = preg_replace_callback('/%%[a-z_-]*%%/',
                        function ($matches) use ($data_values) {
                            return $data_values->{str_replace('%%', '', $matches[0])} ?? 0;
                        },
                        $attribute->calculation);
                    
                    $parser = new FormulaParser($calc, 10);
                    $result = $parser->getResult();
                    $value = $result[0] == 'done' ? $result[1] : 0;
                }
        
                if($attribute->round != NULL) {
                    $value = round($value, $attribute->round);
                }

                $values[$attribute->key] = $value;
            }
        }

        date_default_timezone_set('Europe/Amsterdam');
        if(property_exists($data_values, 'epoch') && $data_values->epoch > 0) {
            $created_at = date('Y-m-d H:i:s', $data_values->epoch);
        } else if(isset($data['message']['publishTime'])) {
            $created_at = date('Y-m-d H:i:s', strtotime($data['message']['publishTime']));
        } else {
            $created_at = date('Y-m-d H:i:s');
        }

        $log = $vehicle->logs()->create([
            'created_at' => $created_at,
            'values' => $values,
        ]);

        event(new NewLogCreatedEvent($log));
        return $log;
    }
}
