<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Show the vehicle linked to the current user
     *
     * @return \Illuminate\Http\Response
     */
    public function showMyVehicle(Request $request)
    {
        $this->authorize('showMyVehicle', Vehicle::class);
        return $request->user()->vehicle;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => 'max:100',
            'team_name' => 'string|max:100',
        ]);

        $vehicle = $request->user()->vehicle;

        $this->authorize('update', $vehicle);

        $vehicle->update($data);

        return $vehicle;
    }
}
