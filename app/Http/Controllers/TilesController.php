<?php

namespace App\Http\Controllers;

use App\Tile;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TilesController extends Controller
{
    public function viewAny(Request $request)
    {
        $this->authorize('viewAny', Tile::class);

        $vehicle = $request->user()->vehicle;
        return $vehicle->tiles()->orderBy('tiles.sort')->orderBy('tiles.id')->with('attributes')->get();
    }

    
    public function create(Request $request)
    {
        $this->authorize('create', Tile::class);
        
        $data = $request->validate([
            'name' => 'required|max:100',
            'type' => 'required|max:100',
            'public' => 'required|boolean',
            'configuration' => 'required',
            'attributes' => 'required|array',
            'attributes.*' => 'required|string|distinct'
        ]);

        $vehicle = $request->user()->vehicle;

        $filtered_attributes = $vehicle->attributes->filter(function ($obj, $key) use ($data) {
            return in_array($obj['key'], $data['attributes']);
        });
        if(count($filtered_attributes) != count($data['attributes'])) {
            throw ValidationException::withMessages(['attributes' => 'Not all attributes are valid']);
        }

        $tile = $request->user()->vehicle->tiles()->create($data);
        $tile->attributes()->attach($filtered_attributes);

        return $tile;
    }

    
    public function update(Request $request, int $id)
    {
        $data = $request->validate([
            'name' => 'required|max:100',
            'type' => 'required|max:100',
            'public' => 'required|boolean',
            'configuration' => 'required',
            'attributes' => 'required|array',
            'attributes.*' => 'required|string|distinct'
        ]);

        $tile = Tile::find($id);

        $this->authorize('update', $tile);

        $filtered_attributes = $tile->vehicle->attributes->filter(function ($obj, $key) use ($data) {
            return in_array($obj['key'], $data['attributes']);
        });
        if(count($filtered_attributes) != count($data['attributes'])) {
            throw ValidationException::withMessages(['attributes' => 'Not all attributes are valid']);
        }

        $tile->update($data);
        $tile->attributes()->sync($filtered_attributes);

        return $tile;
    }
}
