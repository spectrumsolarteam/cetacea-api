<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;

class PublicVehicleController extends Controller
{
    public function viewAttributes(Request $request, $channel)
    {
        $vehicle = Vehicle::where('public_channel', $channel)->first();
        return $vehicle->attributes()->public()->get();
    }

    public function viewTiles(Request $request, $channel)
    {
        $vehicle = Vehicle::where('public_channel', $channel)->first();
        return $vehicle->tiles()->public()->orderBy('tiles.sort')->orderBy('tiles.id')->with('attributes')->get();
    }
}
