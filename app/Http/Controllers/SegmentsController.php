<?php

namespace App\Http\Controllers;

use App\Segment;
use Illuminate\Http\Request;
use App\Exports\SegmentExport;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use MathPHP\Statistics\Regression;

class SegmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewAny(Request $request)
    {

        return $request->user()->vehicle->segments()->orderByDesc('start')->get();
    }

    public function view(Request $request, int $id)
    {
        $segment = Segment::find($id);

        $this->authorize('view', $segment);

        return $segment;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Segment::class);

        $data = $request->validate([
            'name' => 'required|max:100',
            'start' => 'required',
            'end' => 'required'
        ]);

        $segment = $request->user()->vehicle->segments()->create($data);

        return $segment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $data = $request->validate([
            'name' => 'required|max:100',
            'start' => 'required',
            'end' => 'required'
        ]);

        $segment = Segment::find($id);

        $this->authorize('update', $segment);

        $segment->update($data);

        return $segment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Segment $segment)
    {
        //
    }

    public function export(Request $request, int $id)
    {
        $segment = Segment::find($id);

        $cols = ['logs.created_at as timestamp'];
        foreach($segment->vehicle->attributes as $attribute) {
            $cols[] = 'logs.values->'.$attribute->key.' as '.$attribute->key;
        }

        function logsGenerator($segment, $cols): \Generator
        {
            foreach ($segment->logs()->select($cols)->cursor() as $user) {
                yield $user;
            }
        }

        return (new FastExcel(logsGenerator($segment, $cols)))->download('Segment data '.$segment->name.'.csv');
    }

    public function getTimeSeries(Request $request, int $id)
    {
        $segment = Segment::with('attrs')->findOrFail($id);
        $this->authorize('view', $segment);

        $requested_attributes = array_values(array_filter(explode(',', $request->query('attributes'))));
        sort($requested_attributes);

        array_filter($requested_attributes, function($a) use ($segment) {
            return $segment->attrs->contains('key', $a);
        });

        throw_if(count($requested_attributes) < 1, new \Exception('Specify at least one attribute'));

        // todo: (for later) aggregate data if set becomes too large

        $cols = ['logs.created_at as created_at'];
        foreach($requested_attributes as $attribute) {
            $cols[] = 'logs.values->'.$attribute.' as '.$attribute;
        }
        $logs = $segment->logs()->select($cols)->orderBy('created_at')->get();

        $return = [];
        foreach($requested_attributes as $key) {
            $attr = $segment->attrs->firstWhere('key', $key) ?? null;
            $return[$key]['name'] = isset($attr) ?
                                            $attr->name . ' [' . $attr->unit . ']' :
                                            $key;
        }
        foreach($logs as $log) {
            foreach($requested_attributes as $attribute) {
                $return[$attribute]['data'][] = [
                    $log['created_at'],
                    ($log[$attribute] === "") ? null : $log[$attribute]
                ];
            }
        }

        return array_values($return);
    }

    public function getScatter(Request $request, int $id)
    {
        $segment = Segment::with('attrs')->findOrFail($id);
        $this->authorize('view', $segment);

        $x_attribute = $request->query('x');
        $y_attribute = $request->query('y');

        throw_if(!$x_attribute, new \Exception('Attribute for x axis is required'));
        throw_if(!$y_attribute, new \Exception('Attribute for y axis is required'));
        throw_if(!$segment->attrs->contains('key', $x_attribute), new \Exception('Attribute for x is invalid'));
        throw_if(!$segment->attrs->contains('key', $y_attribute), new \Exception('Attribute for y is invalid'));

        $cols = ['logs.created_at as created_at'];
        foreach([$x_attribute, $y_attribute] as $attribute) {
            $cols[] = 'logs.values->'.$attribute.' as '.$attribute;
        }
        $logs = $segment->logs()->select($cols)->orderBy('created_at')->get();

        $return = [];
        $points_for_regression = [];
        foreach($logs as $log) {
            try {
                $x = (float) $log[$x_attribute];
                $y = (float) $log[$y_attribute];
                $return['points'][] = ['x'=>$x, 'y'=>$y];
                $points_for_regression[] = [$x, $y];
            } catch (\Exception $e) {
                // do nothing
            }
        }


        $alpha = 1/3;   // Smoothness parameter
        $lambda = 1;    // Order of the polynomial fit
        $regression = new Regression\LOESS($points_for_regression, $alpha, $lambda);

        $xs = array_unique($regression->getXs());
        sort($xs);
        $min = min($xs);
        $max = max($xs);
        foreach(range($min, $max, ($max-$min)/30) as $x_point) { // done to limit the amount of evaluations, as the computation time can get above the max quite easily.
            $return['regressed'][] = [
                $x_point,
                $regression->evaluate($x_point)
            ];
        }

        return $return;
    }
}
