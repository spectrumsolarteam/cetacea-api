<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Vehicle;
use App\RealtimeToken;

class RealtimeController extends Controller
{
    public function createToken(Request $request)
    {
        $vehicle = $request->user()->vehicle;

        $token = Str::random(64);
        RealtimeToken::create([
            'token' => $token,
            'channel' => $vehicle->private_channel,
            'valid' => true,
        ]);

        return $token;
    }

    public function registerToPrivateChannel(Request $request, String $token)
    {
        $realtime_token = RealtimeToken::where('token', $token)->first();
        if(!$realtime_token) {
            throw new \Exception('Token invalid', 401);
        }

        if(!$realtime_token->isValid()) {
            throw new \Exception('Token invalid', 401);
        }

        $realtime_token->update([
            'valid' => false,
        ]);

        return response('', 200)
                  ->header('Content-Type', 'text/event-stream')
                  ->header('Grip-Hold', 'stream')
                  ->header('Grip-Keep-Alive', ':\n\n; format=cstring; timeout=20')
                  ->header('Grip-Channel', $realtime_token->channel);
    }

    public function registerToPublicChannel(Request $request, String $channel)
    {
        if(!Vehicle::where('public_channel', $channel)->exists()) {
            throw new \Exception('Channel does not exist', 404);
        }

        return response('', 200)
                  ->header('Content-Type', 'text/event-stream')
                  ->header('Grip-Hold', 'stream')
                  ->header('Grip-Keep-Alive', ':\n\n; format=cstring; timeout=20')
                  ->header('Grip-Channel', $channel);
    }
}
