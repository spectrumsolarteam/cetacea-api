<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use App\Vehicle;

class LogsController extends Controller
{
    public function getLatest(Request $request)
    {
        $user = $request->user();
        $log = $user->logs()->latest()->first();

        $this->authorize('view', $log);
        return $log;
    }

    public function createFromFile(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:txt,json',
        ]);

        $this->authorize('create', Log::class);

        date_default_timezone_set('Europe/Amsterdam');
        $content = $request->file('file')->get();
        $vehicle = $request->user()->vehicle()->with('attributes')->first();
        $logs = [];

        $separator = "\r\n";
        $line = strtok($content, $separator);
        while ($line !== false) {
            $data = json_decode($line);

            if(($data->epoch ?? 0) > 0) {
                $created_at = date('Y-m-d H:i:s', $data->epoch);
                $count = Log::ofVehicle($vehicle)->where('created_at', $created_at)->count();
                if($count == 0) {
                    $logs[] = [
                        'values' => Log::calculateValues($vehicle, $data),
                        'created_at' => $created_at
                    ];
                }
            }

            // parse next line;
            $line = strtok($separator);
        }

        $vehicle->logs()->createMany($logs);

        return Response(null, 201);
    }
}
