<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attributes';
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    protected $fillable = [
        'key', 'name', 'public', 'unit', 'round', 'upper_bound', 'lower_bound', 'calculation', 'archived'
    ];

    protected $visible = [
        'key', 'name', 'public', 'unit', 'round', 'upper_bound', 'lower_bound', 'calculation', 'archived', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'archived' => 'boolean',
    ];

    protected $attributes = [
        'archived' => false,
        'round' => 0,
        'public' => false
    ];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function scopeUnarchived($query)
    {
        return $query->where('archived', false);
    }

    public function scopePublic($query)
    {
        return $query->where('public', true);
    }
}
