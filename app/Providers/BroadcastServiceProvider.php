<?php

namespace App\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;
use App\Broadcasting\FanoutBroadcaster;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make(
            'Illuminate\Broadcasting\BroadcastManager')->extend(
            'fanout', function ($app, $config) {
                return new FanoutBroadcaster($config);
        });
        
        // Broadcast::routes();

        // require base_path('routes/channels.php');
    }
}
