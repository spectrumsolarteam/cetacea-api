<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tile extends Model
{
    //
    protected $table = 'tiles';

    protected $casts = [
        'configuration' => 'object'
    ];

    protected $fillable = [
        'name', 'type', 'public', 'configuration'
    ];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function attributes()
    {
        return $this->belongsToMany('App\Attribute', 'tiles_attributes', 'tile_id', 'attribute_id');
    }

    public function scopePublic($query)
    {
        return $query->where('public', true);
    }

    /**
     * I hate this, but somehow $tile->attributes doesn't give the related App\Attributes, where $vehicle->attributes does...
     */
    public function getRelatedAttributes() {
        return $this->relations['attributes'] ?? [];
    }
}
